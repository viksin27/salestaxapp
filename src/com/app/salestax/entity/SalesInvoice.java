package com.app.salestax.entity;

/**
 * Class to hold the information of Invoice
 * 
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */
public class SalesInvoice {

	private OrderItems orderItems;
	private Double totalTaxAmount = 0.00;
	private Double totalSalesAmount = 0.00;
	private Double totalItemsAmount = 0.00;

	/**
	 * Invoking with order items
	 * 
	 * @param orderItems
	 *            order items for the invoice
	 */
	public SalesInvoice(final OrderItems orderItems) {
		this.orderItems = orderItems;
	}

	public Double getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(final Double totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	public Double getTotalSalesAmount() {
		totalSalesAmount = this.getTotalItemsAmount()
				+ this.getTotalTaxAmount();
		return totalSalesAmount;
	}

	public void setTotalSalesAmount(final Double totalSalesAmount) {
		this.totalSalesAmount = totalSalesAmount;
	}

	public Double getTotalItemsAmount() {
		return totalItemsAmount;
	}

	public void setTotalItemsAmount(final Double totalItemsAmount) {
		this.totalItemsAmount = totalItemsAmount;
	}

	public OrderItems getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(final OrderItems orderItems) {
		this.orderItems = orderItems;
	}

}
