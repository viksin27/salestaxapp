package com.app.salestax.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * This class holds details of the order items
 * 
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */
public class OrderItems {

	private List<Item> shoppingCart = new ArrayList<Item>();

	/**
	 * This method adds a new item with specified quantity, its details and price
	 * 
	 * @param quantity
	 *            - quantity of the item to be added
	 * @param details
	 *            - details of the item
	 * @param price
	 *            - price of item
	 * @return item added to the cart
	 */
	public Item addItem(final int quantity, final String details, final Double price) {
		Item item = new Item(quantity, details, price);
		this.shoppingCart.add(item);
		return item;
	}

	/**
	 * This method returns the list of items present in the cart
	 * 
	 * @return items list in cart
	 */
	public List<Item> getShoppingCart() {
		return this.shoppingCart;
	}

}
