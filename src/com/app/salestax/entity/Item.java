package com.app.salestax.entity;

import java.util.regex.Pattern;

/**
 * Entity class for holding information related to items
 * 
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */
public class Item {

	private int quantity;
	private String details;
	private Double price;
	private Boolean isImport = false;
	private Boolean isExempt = false;
	private Double afterTaxPrice;

	/**
	 * 
	 * @param quantity
	 * @param details
	 * @param price
	 */
	public Item(final int quantity, final String details, final Double price) {
		this.quantity = quantity;
		this.details = details;
		this.price = price;
		setSaleType(details);
	}

	/**
	 * returns the quantity for the item
	 * 
	 * @return item quantity
	 */
	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * Returns the details for the item
	 * 
	 * @return item details
	 */
	public String getDetails() {
		return this.details;
	}

	/**
	 * Returns the price of item
	 * 
	 * @return price of an item
	 */
	public Double getPrice() {
		return this.price;
	}

	/**
	 * Is the item exempted from tax
	 * 
	 * @return if the item exempted
	 */
	public Boolean isExempt() {
		return this.isExempt;
	}

	/**
	 * Is the item imported
	 * 
	 * @return if the item imported
	 */
	public Boolean isImport() {
		return this.isImport;
	}

	/**
	 * Returns after tax price of item
	 * 
	 * @return after tax price
	 */
	public Double getAfterTaxPrice() {
		return this.afterTaxPrice;
	}

	/**
	 * Sets after tax price
	 * 
	 * @param afterTaxPrice
	 */
	public void setAfterTaxPrice(final Double afterTaxPrice) {
		this.afterTaxPrice = afterTaxPrice;
	}

	/**
	 * Sets the sales type
	 * 
	 * @param details
	 */
	private void setSaleType(final String details) {
		Pattern exemptPattern = Pattern.compile("pills|chocolate|book");
		Pattern importPattern = Pattern.compile("imported");
		if (exemptPattern.matcher(details).find()) {
			this.isExempt = true;
		}

		if (importPattern.matcher(details).find()) {
			this.isImport = true;
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((afterTaxPrice == null) ? 0 : afterTaxPrice.hashCode());
		result = prime * result + ((details == null) ? 0 : details.hashCode());
		result = prime * result
				+ ((isExempt == null) ? 0 : isExempt.hashCode());
		result = prime * result
				+ ((isImport == null) ? 0 : isImport.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + quantity;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (afterTaxPrice == null) {
			if (other.afterTaxPrice != null)
				return false;
		} else if (!afterTaxPrice.equals(other.afterTaxPrice))
			return false;
		if (details == null) {
			if (other.details != null)
				return false;
		} else if (!details.equals(other.details))
			return false;
		if (isExempt == null) {
			if (other.isExempt != null)
				return false;
		} else if (!isExempt.equals(other.isExempt))
			return false;
		if (isImport == null) {
			if (other.isImport != null)
				return false;
		} else if (!isImport.equals(other.isImport))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (quantity != other.quantity)
			return false;
		return true;
	}
}
