package com.app.salestax.order;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import com.app.salestax.entity.OrderItems;
import com.app.salestax.util.STUtil;

/**
 * This class is responsible for reading the shopping cart list items from the
 * input *.txt file
 * 
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */
public class OrderItemsReader {

	public static final String PATTERN_ITEM_QUANTITY = "^[\\d+]+";
	public static final String PATTERN_ITEM_DETAIL = "(?!^\\d)[A-Za-z].+(?=\\sat\\s\\d+.\\d+$)";
	public static final String PATTERN_ITEM_PRICE = "\\d+.\\d+$";

	/**
	 * 
	 */
	private OrderItems orderItems;

	/**
	 * Initialize the scanner with loading the shopping cart items mentioned in
	 * the file provided at the specified location
	 * 
	 * @param shoppingCartInputPath
	 *            - path to the shopping cart list item file
	 */
	public OrderItemsReader(final String shoppingCartInputPath) {
		// Create buffered reader for reading the file
		try (FileReader fileReader = new FileReader(shoppingCartInputPath);
				BufferedReader reader = new BufferedReader(fileReader)) {
			String itemDetailsLine;
			this.orderItems = new OrderItems();
			while ((itemDetailsLine = reader.readLine()) != null) {
				// prepare order items
				this.prepareOrderItems(itemDetailsLine);
			}
		} catch (IOException e) {
			System.err.println("Error: " + e.getMessage());
		}
	}

	/**
	 * Returs the order items
	 * 
	 * @return order items
	 */
	public OrderItems getOrderItems() {
		return this.orderItems;
	}

	/**
	 * This method prepares order items using the item line
	 * 
	 * @param itemLine
	 *            item line for the order items
	 */
	private void prepareOrderItems(final String itemLine) {
		// parse the item quantity for the item
		String strItemQuantity = STUtil.parseLineForOrderItemData(
				PATTERN_ITEM_QUANTITY, itemLine);
		int itemQuantity = 0;
		if (STUtil.isStringNotEmpty(strItemQuantity)) {
			itemQuantity = Integer.parseInt(strItemQuantity);
		}
		// parse the line for item details
		String strItemDetails = STUtil.parseLineForOrderItemData(
				PATTERN_ITEM_DETAIL, itemLine);

		// parse the line for item price
		String strItemPrice = STUtil.parseLineForOrderItemData(
				PATTERN_ITEM_PRICE, itemLine);
		double itemPrice = 0d;
		if (STUtil.isStringNotEmpty(strItemPrice)) {
			itemPrice = Double.parseDouble(strItemPrice);
		}
		// add the item to orderItems
		this.orderItems.addItem(itemQuantity, strItemDetails, itemPrice);

	}
	
}
