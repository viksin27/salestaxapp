package com.app.salestax;

import com.app.salestax.entity.Item;
import com.app.salestax.entity.OrderItems;
import com.app.salestax.entity.SalesInvoice;
import com.app.salestax.util.STUtil;

/**
 * Calculator for Sales tax and totals for the items
 * 
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */
public class SalesTaxCalculator {

	private static final SalesTaxCalculator _CALCULTOR = new SalesTaxCalculator();

	/**
	 * Invokes the calculator implementation and returns a singleton instance
	 * 
	 * @return _CALCULTOR {@link SalesTaxCalculator} instance
	 */
	public static final SalesTaxCalculator getInstance() {
		return _CALCULTOR;
	}

	/**
	 * This function computes sales tax and generates the invoice
	 * 
	 * @param orderItems
	 *            - order items
	 * @return invoice for the order
	 */
	public SalesInvoice calculateAndGenerateSalesInvoiceData(
			final OrderItems orderItems) {
		if (orderItems == null) {
			System.err.println("Order items details not found");
			return null;
		}
		SalesInvoice salesInvoice = new SalesInvoice(orderItems);
		for (Item item : orderItems.getShoppingCart()) {
			Double itemPriceTotal = item.getPrice() * item.getQuantity();
			salesInvoice.setTotalItemsAmount(salesInvoice.getTotalItemsAmount()
					+ itemPriceTotal);
			salesInvoice.setTotalTaxAmount(salesInvoice.getTotalTaxAmount()
					+ computeSalesTax(item));
		}
		return salesInvoice;
	}

	/**
	 * Computes sales tax for an item
	 * 
	 * @param item
	 *            - item for which sales tax needs to be calculated
	 * @return the sales tax for the item
	 */
	private Double computeSalesTax(final Item item) {
		// default tax amount
		Double tax = .10;
		// check if the item is exempted from tax
		if (item.isExempt()) {
			tax = .00;
		}
		// check if the item is imported
		if (item.isImport()) {
			// add an additional tax of 5% on imported items
			tax = tax + .05;
		}
		// rounded for the tax on item for the specified quantity
		Double rounded = STUtil.roundAmount((item.getPrice() * tax)
				* item.getQuantity());
		item.setAfterTaxPrice(rounded + (item.getPrice() * item.getQuantity()));
		return rounded;
	}
}
