package com.app.salestax.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This util class is responsible to check whether or not a string is
 * null/empty, to parse lines for ordered items from .txt files and for rounding
 * off of amount.
 *
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */

public class STUtil {

	/**
	 * Check is the string path is not null and not empty
	 * 
	 * @param stringToCheck
	 *            - string to be validated
	 * @return if the string path is not null and not empty
	 */
	public static final boolean isStringNotEmpty(final String stringToCheck) {
		return stringToCheck != null && !stringToCheck.isEmpty();
	}

	/**
	 * parses line as per the pattern and provides the data
	 * 
	 * @param pattern
	 *            - pattern for which to be looked
	 * @param itemLine
	 *            - item line to be read
	 * @return itemData - data returned as per the pattern
	 */
	public static final String parseLineForOrderItemData(final String pattern,
			final String itemLine) {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(itemLine);
		m.find();
		return m.group(0);
	}

	/**
	 * rounds the amount provided
	 * 
	 * @param amount
	 *            amount to be rounded off
	 * @return rounded amount
	 */
	public static final Double roundAmount(final Double amount) {
		if (amount == null) {
			return null;
		}
		return Math.ceil((amount * 20.0)) / 20.0;
	}
}
