package com.app.salestax.util;

import java.util.List;

import com.app.salestax.entity.Item;
import com.app.salestax.entity.SalesInvoice;

/**
 * This class is responsible for printing the output to the console. Created a
 * singleton instance as this class is used for printing only and single object
 * could be used to do the job
 * 
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */
public class SalesRecieptConsolePrinter {
	/**
	 * Singleton object for the printer class
	 */
	private static final SalesRecieptConsolePrinter _ME = new SalesRecieptConsolePrinter();

	/**
	 * Formats defined for printing data on console
	 */
	private static final String ITEM_LINE_FORMAT = "%1$-40s %2$6.2f %n";
	private static final String TAX_LINE_FORMAT = "%1$40s %2$6.2f %n";
	private static final String TOTAL_LINE_FORMAT = "%1$40s %2$6.2f %n%n%n";

	/**
	 * Private constructor, to restrict creation of objects from outside this
	 * class
	 */
	private SalesRecieptConsolePrinter() {

	}

	/**
	 * Static method to access the singleton object for this class
	 * 
	 * @return _ME {@link SalesRecieptConsolePrinter} instance
	 */
	public static final SalesRecieptConsolePrinter getInstance() {
		return _ME;
	}

	/**
	 * This method is used to print the receipt for the sales
	 * 
	 * @param salesInvoice
	 *            invoice to be printed
	 */
	public void printReceipt(final SalesInvoice salesInvoice) {
		if (salesInvoice == null) {
			System.err
					.println("Error while printing sales invoice. found no data.");
			return;
		}
		if (salesInvoice.getOrderItems() == null) {
			System.err
					.println("Error while printing sales invoice. OrderItems is blank.");
			return;
		}
		List<Item> shoppingCart = salesInvoice.getOrderItems()
				.getShoppingCart();
		// print the list of items in cart one by one`
		for (Item item : shoppingCart) {
			this.printItemList(item);
		}
		// put a line break in between
		this.lineBreak();
		// print the sales tax line
		this.printSalesTax(salesInvoice.getTotalTaxAmount());
		// print total sales amount line
		this.printTotalSale(salesInvoice.getTotalSalesAmount());
	}

	/**
	 * Print the item to the console
	 * 
	 * @param item
	 *            - item to be printed
	 */
	private void printItemList(final Item item) {
		if (item == null) {
			// don't print anything here as the item is null
			return;
		}
		System.out.format(ITEM_LINE_FORMAT,
				item.getQuantity() + " " + item.getDetails() + ": ",
				item.getAfterTaxPrice());
	}

	/**
	 * Print the sales tax to the console
	 * 
	 * @param item
	 *            - item to be printed
	 */
	private void printSalesTax(final Double taxTotal) {
		if (taxTotal == null) {
			System.out.format(TAX_LINE_FORMAT, "Sales Taxes:", 0d);
			return;
		}
		System.out.format(TAX_LINE_FORMAT, "Sales Taxes:", taxTotal);
	}

	/**
	 * Print the sale to the console
	 * 
	 * @param item
	 *            - sale amount to be printed
	 */
	private void printTotalSale(final Double saleTotal) {
		if (saleTotal == null) {
			System.out.format(TOTAL_LINE_FORMAT, "Total:", 0d);
			return;
		}
		System.out.format(TOTAL_LINE_FORMAT, "Total:", saleTotal);
	}

	/**
	 * Print the breaks to the console
	 * 
	 */
	private void lineBreak() {
		String dashes = new String(new char[48]).replace("\0", "-");
		System.out.format(dashes + "%n");
	}
}
