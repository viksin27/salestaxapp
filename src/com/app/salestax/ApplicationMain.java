package com.app.salestax;

import java.io.File;
import java.io.IOException;

import com.app.salestax.entity.OrderItems;
import com.app.salestax.entity.SalesInvoice;
import com.app.salestax.order.OrderItemsReader;
import com.app.salestax.util.SalesRecieptConsolePrinter;

/**
 * This class is the entry point of the application
 * 
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */
public class ApplicationMain {

	public static void main(String[] args) throws IOException {

		final String[] defaultShoppingCarts = { "src/resources/input1.txt",
				"src/resources/input2.txt" };

		String[] queuedCartFiles = (args.length > 0) ? args
				: defaultShoppingCarts;

		// iterate over the cart file one by one
		for (String cartItemsFilePath : queuedCartFiles) {
			// read the file
			File file = new File(cartItemsFilePath);
			// if file exists then only process the items
			if (file.exists()) {
				// read the items
				OrderItemsReader orderItemsReader = new OrderItemsReader(
						cartItemsFilePath);
				// Order items purchased
				OrderItems orderItems = orderItemsReader.getOrderItems();

				// Invokes the sales calculator
				SalesTaxCalculator salesTaxCalculator = SalesTaxCalculator
						.getInstance();
				// do the computation and generate sales invoice
				SalesInvoice invoice = salesTaxCalculator
						.calculateAndGenerateSalesInvoiceData(orderItems);

				// Load the console printer
				SalesRecieptConsolePrinter receiptPrinter = SalesRecieptConsolePrinter
						.getInstance();
				// print the receipt
				receiptPrinter.printReceipt(invoice);

			}
		}
	}
}
