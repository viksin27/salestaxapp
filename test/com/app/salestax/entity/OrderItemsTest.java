package com.app.salestax.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class OrderItemsTest {
	private OrderItems orderItems;

	@Before
	public void setUp() {
		orderItems = new OrderItems();
	}

	@Test
	public void testGetShoppingCartSizeAfterAdditionOfItem() throws Exception {
		// given Order items instantiated
		// when an item is added to the order items list
		orderItems.addItem(1, "imported box of chocolates", 10.00);
		// then the size of the order item must be 1
		assertEquals(1, orderItems.getShoppingCart().size());
	}

	@Test
	public void testAddItemReturnsTheSameObject() throws Exception {
		// given an item is added to the order Items
		Item item = orderItems.addItem(1, "imported box of chocolates", 10.00);
		// then the returned item must be same as the provided Item
		assertEquals(item, new Item(1, "imported box of chocolates", 10.00));
	}

	@Test
	public void testAddItemForValidQuantity() throws Exception {
		// given order items is added an item with valid quantity
		Integer validQuantity = 1;
		// when an item is added to the order items with the given quantity
		Item item = orderItems.addItem(validQuantity,
				"imported box of chocolates", 10.00);
		// then the item object return must have the same quantity as given
		assertTrue(validQuantity.equals(item.getQuantity()));
	}

	@Test
	public void testAddItemForValidDetails() throws Exception {
		// given order items is added an item with valid details
		String givenDetails = "imported box of chocolates";
		// when an item is added to the order items with the given details
		Item item = orderItems.addItem(1, givenDetails, 10.00);
		// then the item object return must have the same details as given
		assertEquals(givenDetails, item.getDetails());
	}

	@Test
	public void testAddItemForValidPrice() throws Exception {
		// given order items is added an item with valid price
		Double givenPrice = 10.00;
		Item item = orderItems.addItem(1, "imported box of chocolates",
				givenPrice);
		// the price expected from the returned object must match the given
		// value
		assertTrue(givenPrice.equals(item.getPrice()));
	}

	@Test
	public void testGetShoppingCart() throws Exception {
		// given the class is only instantiated and no process is performed on
		// the class
		// then the shopping cart list must not be null
		assertNotNull(orderItems.getShoppingCart());
	}
}