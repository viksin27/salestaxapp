package com.app.salestax.entity;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class ItemTest {
	private Item item;
	private int quantity;
	private String details;
	private Double price;

	@Before
	public void setUp() {
		quantity = 1;
		details = "music CD";
		price = 14.99;
		// given the item is not exempted and not imported as per the details
		// provided
		item = new Item(quantity, details, price);
	}

	@Test
	public void testGetQuantityReturnValid() throws Exception {
		// given item instantiated
		// when getQuantity method is called, it must return the value fed while
		// initializing
		assertEquals(quantity, item.getQuantity());
	}

	@Test
	public void testGetDetailsReturnValid() throws Exception {
		// given item instantiated
		// when getDetails method is called, it must return the value fed while
		// initializing
		assertEquals(details, item.getDetails());
	}

	@Test
	public void testGetPriceReturnValid() throws Exception {
		// given item instantiated
		// when getPrice method is called, it must return the value fed while
		// initializing
		assertEquals(price, item.getPrice());
	}

	@Test
	public void testIsExemptPropertyNonExemptedItem() throws Exception {
		// given a string with details
		String exemptDetails = "pills and food";
		// when item is instantiated with the details
		Item exemptItem = new Item(quantity, exemptDetails, price);
		// the item must return true while isExempt method is called
		assertEquals(true, exemptItem.isExempt());

	}

	@Test
	public void testIsExemptPropertyForNonExemptedItem() throws Exception {
		// the item must return false if the items is not exempted when isExempt
		// method is called
		assertEquals(false, item.isExempt());
	}

	@Test
	public void testIsImportPropertyForImportedItem() throws Exception {
		// given a string with details
		String importDetails = "imported stuff";
		// when item is instantiated with the details
		Item exemptItem = new Item(quantity, importDetails, price);
		// the item must return true while isImport method is called
		assertEquals(true, exemptItem.isImport());

	}

	@Test
	public void testIsImportPropertyForNonImportedItem() throws Exception {
		// the item must return false if the items is not imported when isExempt
		// method is called
		assertEquals(false, item.isImport());
	}

	@Test
	public void textGetAfterTaxPrice() throws Exception {
		// given after tax amount is 2.00
		Double taxAmount = 2.00;
		// when the method is invoked to set the after tax price
		item.setAfterTaxPrice(taxAmount);
		// then the amount must return the same value from the method
		// getAfterTaxPrice
		assertEquals(taxAmount, item.getAfterTaxPrice());
	}

	@Test
	public void testGetAfterTaxPriceIfNotSet() throws Exception {
		// given when after tax amount is not set
		// then the amount must return the null value from the method
		// getAfterTaxPrice
		assertEquals(null, item.getAfterTaxPrice());
	}

}