package com.app.salestax;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.BeforeClass;
import org.junit.Test;

import com.app.salestax.entity.OrderItems;
import com.app.salestax.entity.SalesInvoice;

/**
 * Test Implementation for {@link SalesTaxCalculator} implementation
 * 
 * @author Vikash Singh(vikash.singh271980@gmail.com)
 *
 */
public class SalesTaxCalculatorTest {

	/**
	 * Sales Tax calculator object
	 */
	private static SalesTaxCalculator salesTaxCalculator;

	@BeforeClass
	public static void setUp() {
		salesTaxCalculator = SalesTaxCalculator.getInstance();
	}

	@Test
	public void testGetTotalSalesAmountWithValidData() throws Exception {
		// given
		OrderItems orderItems = new OrderItems();

		// when appropriate data is found and sales computation is done
		orderItems.addItem(1, "book", 12.49);
		orderItems.addItem(1, "music CD", 14.99);
		orderItems.addItem(1, "chocolate bar", 0.85);
		SalesInvoice salesInvoice = salesTaxCalculator
				.calculateAndGenerateSalesInvoiceData(orderItems);

		// then the expected values must match
		Double expectedTotalSalesAmount = 29.83;
		Double totalSalesAmount = (double) Math.round(salesInvoice
				.getTotalSalesAmount() * 100d) / 100d;

		assertEquals(expectedTotalSalesAmount, totalSalesAmount);
	}

	@Test
	public void testGetTotalTaxAmountWithValidData() throws Exception {
		// given
		OrderItems orderItems = new OrderItems();

		// when appropriate data is found
		orderItems.addItem(1, "book", 12.49);
		orderItems.addItem(1, "music CD", 14.99);
		orderItems.addItem(1, "chocolate bar", 0.85);
		SalesInvoice salesInvoice = salesTaxCalculator
				.calculateAndGenerateSalesInvoiceData(orderItems);

		// then the expected total tax amount must match
		Double expectedTotalTaxAmount = 1.50;
		Double totalTaxAmount = salesInvoice.getTotalTaxAmount();

		assertEquals(expectedTotalTaxAmount, totalTaxAmount);
	}

	@Test
	public void testCalculateAndGenerateSalesInvoiceDataWithNoItemsAddedToOrder()
			throws Exception {
		// given order items initialized and no data is added
		OrderItems orderItems = new OrderItems();
		// when calculateAndGenerateSalesInvoiceData is called
		SalesInvoice salesInvoice = salesTaxCalculator
				.calculateAndGenerateSalesInvoiceData(orderItems);
		// then the sales invoice must be returned
		assertNotNull(salesInvoice);
	}

	@Test
	public void testCalculateAndGenerateSalesInvoiceDataWithNullOrderItems()
			throws Exception {
		// given null order items
		OrderItems orderItems = null;
		// when computation is invoked
		SalesInvoice salesInvoice = salesTaxCalculator
				.calculateAndGenerateSalesInvoiceData(orderItems);

		// then the null sales invoice must be returned
		assertNull(salesInvoice);
	}

	@Test
	public void testGetInstance() {
		// given existing object salesTaxCalculator
		// when getInstance is again invoked
		SalesTaxCalculator calculator = SalesTaxCalculator.getInstance();
		// then the both objects must be same
		assertEquals(calculator, salesTaxCalculator);
	}
}
