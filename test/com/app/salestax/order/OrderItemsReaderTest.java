package com.app.salestax.order;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.app.salestax.entity.Item;
import com.app.salestax.entity.OrderItems;

public class OrderItemsReaderTest {
	private static OrderItemsReader orderItemsReader;
	private static OrderItems scannerOrderItems;

	/*
	 * private OrderItems orderItems;
	 * 
	 * private Item item1; private Item item2; private Item item3;
	 */

	@BeforeClass
	public static void setUp() {
		orderItemsReader = new OrderItemsReader("src/resources/input1.txt");
		scannerOrderItems = new OrderItems();
		scannerOrderItems.addItem(1, "book", 12.49);
		scannerOrderItems.addItem(1, "music CD", 14.99);
		scannerOrderItems.addItem(1, "chocolate bar", 0.85);
	}

	@Test
	public void testOrderItemsReaderGetOrderItems() throws Exception {
		// given list of items prepared with the same details as that present in
		// the input file
		List<Item> testInventory = scannerOrderItems.getShoppingCart();
		// when orderItems details is read from the file and parsed
		List<Item> actualInventory = orderItemsReader.getOrderItems()
				.getShoppingCart();
		// both the items prepared through objects and input file must match
		assertEquals(actualInventory, testInventory);
	}

}
