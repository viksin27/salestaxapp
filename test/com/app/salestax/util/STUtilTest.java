package com.app.salestax.util;

import static org.junit.Assert.*;

import org.junit.Test;

import com.app.salestax.order.OrderItemsReader;

public class STUtilTest {

	@Test
	public void testIsStringNotEmptyInvalidString() {
		// given blank string
		String givenString = "";
		// when method is called with the specified string
		boolean retValue = STUtil.isStringNotEmpty(givenString);
		// then the method must returnFalse
		assertFalse(retValue);
	}

	@Test
	public void testIsStringNotEmptyBalidString() {
		// given valid string
		String givenString = "ABC";
		// when method is called with the specified string
		boolean retValue = STUtil.isStringNotEmpty(givenString);
		// then the method must returnFalse
		assertTrue(retValue);
	}

	@Test
	public void testIsStringNotEmptyNullString() {
		// given null string
		String givenString = null;
		// when method is called with the specified string
		boolean retValue = STUtil.isStringNotEmpty(givenString);
		// then the method must returnFalse
		assertFalse(retValue);
	}

	@Test
	public void testParseLineForOrderItemData() {
		// given a valid item line
		final String lineItem = "1 book at 12.49";
		// when parse method is called
		String details = STUtil.parseLineForOrderItemData(
				OrderItemsReader.PATTERN_ITEM_DETAIL, lineItem);

		assertEquals("book", details);
	}

	@Test
	public void testRoundAmount() {
		// When the amount is present in the decimal format
		double givenAmount = 10.5390;
		// when the roundAmount method is called
		double roundedAmount = STUtil.roundAmount(givenAmount);
		// the rounded amount must be 10.55
		assertTrue(roundedAmount == 10.55);
	}

	@Test
	public void testRoundAmountNull() {
		// When the amount is present in the decimal format
		Double givenAmount = null;
		// when the roundAmount method is called
		Double roundedAmount = STUtil.roundAmount(givenAmount);
		// the rounded amount must be null
		assertNull(roundedAmount);
	}

}
