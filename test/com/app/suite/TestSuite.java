package com.app.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.app.salestax.SalesTaxCalculatorTest;
import com.app.salestax.entity.ItemTest;
import com.app.salestax.entity.OrderItemsTest;
import com.app.salestax.order.OrderItemsReaderTest;
import com.app.salestax.util.STUtilTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ SalesTaxCalculatorTest.class, ItemTest.class,OrderItemsReaderTest.class,
		STUtilTest.class, OrderItemsTest.class})
public class TestSuite {

}
