# Salestaxapp DOCUMENTATION
___
### SUMMARY

+ **Technical Overview**
+ **Instructions For Install & Use**
+ **Assumptions**        
   1. _File Input_      
   2. _Classes_    
   3. _Tests_


### TECHNICAL OVERVIEW
  
Language: ***Java***        
Core Resources: ***Input ".txt" files***       


### INSTRUCTIONS FOR INSTALL & USE

**Installation in Eclipse**   
    
 1. Clone the project from repository.    
 2. Import project into Eclipse.    
 3. Run *ApplicationMain* class to activate the program.      
 4. Console will display calculated output.    

 **Assumptions**   

File Input|
---|  
A series of filenames can be imported at beginning of program.   
If no filename is provided, 2 existing ".txt" files from resources folder will be loaded.    
The input samples need to be provided as a ".txt" file.    
There need to be a line pattern involved with all input files. Please refer to existing line pattern in input ".txt" files.    
Each line item will contain 3 crucial pieces of information - Quantity. Description. Price.    
The numerical string data needs to be converted to an integer for calculations.    
Pills are considered as medical products.      

Classes |
---|
*ApplicationMain* - This class is an entry point to the application. |  
*SalesTaxCalculator* - This Class is responsible to calculate sales tax and totals. |
*Item* - Entity class for holding information related to items (like description, quantity, tax bracket, and price). |
*OrderItems* - This class holds details of the order items.  |
*SalesInvoice* - View class in charge of providing output to console. | 
*OrderItemsReader* - This class is responsible for reading the shopping cart list items from the input "*.txt" file.  | 
*SalesRecieptConsolePrinter* - This class is responsible for printing the output to the console. Created a singleton instance as this 							   							   class is used for printing only and single object could be used to do the job.   |
*STUtil* - This util class is responsible to check whether or not a string is null/empty, to parse lines for ordered items
  from ".txt" files and for rounding off of amount
  
 Tests |
 ---|
 *SalesTaxCalculatorTest* - Test Implementation for SalesTaxCalculator implementation.  |
 *ItemTest* - Test class for Item.  |
 *OrderItemsTest* - Test class for OrderItems.  |
 *OrderItemsReaderTest* - Test class for OrderItemsReader.  |
 *STUtilTest* - Test class for STUtil.  |
 *TestSuite* - Suite for all the test classes of the application.  |

